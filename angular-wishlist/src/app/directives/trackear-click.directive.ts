import { Directive, ElementRef } from '@angular/core';

import { Store } from '@ngrx/store';

import { fromEvent } from 'rxjs';

import { AppState } from '../app.module';
import { IncrementClicksAction } from '../models/destinos-viajes-state.model';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private element: HTMLInputElement;

  constructor(private elRef: ElementRef, private store: Store<AppState>) {
    this.element = elRef.nativeElement;
    //fromEvent(this.element, 'click').subscribe(evento => this.trackear(evento));
    fromEvent(this.element, 'click').subscribe(() => this.trackear());
  }

  //trackear(evento: Event): void {
  trackear(): void {  
    const elemTags = this.element.attributes.getNamedItem('data-trackear-tags').value.split(' ');
    console.log(`|||||||||| track evento "${elemTags}"`);
    elemTags.forEach(data => this.store.dispatch(new IncrementClicksAction(data)));
  }

}
