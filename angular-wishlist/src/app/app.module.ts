import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ActionReducerMap, StoreModule as NgRxStoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import { Observable, from, of, Subscription } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import Dexie from 'dexie';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { 
  DestinosViajesState,
  initializeDestinosViajesState,
  reducerDestinosViajes,
  DestinosViajesEffects,
  InitMyDataAction
} from './models/destinos-viajes-state.model';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { __metadata } from 'tslib';
import { EspiameDirective } from './directives/espiame.directive';
import { TrackearClickDirective } from './directives/trackear-click.directive';
import { analyzeAndValidateNgModules } from '@angular/compiler';

// app config
export interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// fin app config

// routing
export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponent },
  { path: 'mas-info', component: VuelosMasInfoComponent },
  { path: ':id', component: VuelosDetalleComponent }
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'vuelos',
    component: VuelosComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childrenRoutesVuelos
  }
];
// fin routing

// redux init
export interface AppState {
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

const reducersInitialState = {
  destinos: initializeDestinosViajesState()
};
// fin redux init

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) {}
  async initializeDestinosViajesState(): Promise<any> {
    /*const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'}); // le paso un token de seguridad ficticio porque no tengo nada de login preparado en la api y siempre se va a logar
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', {headers: headers});
    const response: any = await this.http.request(req).toPromise(); // se podría haber hecho con un Subscribe, pero ésta es otra manera de hacerlo, declarando esta función como async, y con un await de una Promise
    this.store.dispatch(new InitMyDataAction(response.body)); // me guardo en mi estado de Redux los ítems que me devuelve la api
    */
    const miDB = db;
    miDB.destinos.toArray().then(items => {
      this.store.dispatch(new InitMyDataAction(items));
    });
  }
}
// fin app init

// dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor () {
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl'
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  }
}

export const db = new MyDatabase();
// fin dexie db

// i18n ini
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) {}

  getTranslation(lang: string): Observable<any> { 
    /*const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                          if (results.length === 0) {
                              return this.http
                                  .get<Translation[]>(
                                      APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang  
                                  )
                                  .toPromise()
                                  .then(apiResults => {
                                      //db.translations.bulkAdd(apiResults);
                                      return apiResults;
                                  });
                          }
                          return results;
                      }).then((traducciones) => {
                          console.log('Traducciones cargadas: ');
                          console.log(traducciones);
                          return traducciones;
                      }).then((traducciones) => {
                          return traducciones.map((t) => ({[t.key]: t.value}));
                      });*/
    //como esta función debe devolver un Observable, y no un Promise, hago el siguiente return from(promise) --> el from convierte a Observable el Promise
    //y el operador flatMap es para convertir el array de arrays, a un array de Translations
    //promise.then(x => console.log('La promesa --> ', x));
    //return from(promise).pipe(flatMap((elems) => from(elems)));

    const promise = this.http
                      .get<Translation[]>(
                          APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang  
                      )
                      .toPromise()
                      .then(apiResults => {
                          //db.translations.bulkAdd(apiResults);
                          return apiResults;
                      })
    .then((traducciones) => {
        console.log('Traducciones cargadas: ');
        console.log(traducciones);
        return traducciones;
    }).then((traducciones) => {
        const trs = traducciones.map((t) => ({[t.key]: t.value}));
        console.log('trads ==> ', trs);
        let aux = "{";
        let md = {};
        //return traducciones.map((t) => ({[t.key]: t.value}));
        traducciones.map((t) => {
          if (aux !== "{") {
            aux = aux + ", ";
          }
          aux = aux + '"' + t.key + '": "' + t.value + '"';
        });
        aux = aux + "}";
        console.log('aux: ', aux);
        md = JSON.parse(aux);
        console.log('json parseee ---> ', JSON.parse(aux));
        console.log('md --> ', md);
        return md;
    });
    promise.then(x => console.log('La promesa --> ', x));
    return from(promise);
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
// i18n fin

// servicio que actualiza contadores
export function init_app2(contadoresService: ContadoresService): () => Subscription {
  return () => contadoresService.observarCambiosContadores();
}

@Injectable()
export class ContadoresService {
  constructor(private store: Store<AppState>, private http: HttpClient) {}

  observarCambiosContadores(): Subscription {
    //this.store.select(state => state.destinos).toPromise().then(data => {
    return this.store.select(state => state.destinos).subscribe(data => {
      console.log(`Observo cambios contadores --> lista: ${data.lista_destinos_item}, prefe: ${data.destino_elegir_preferido}`);
      this.enviarCambiosContadores(data.lista_destinos_item, data.destino_elegir_preferido);
    });
  }

  async enviarCambiosContadores(lista: number, prefe: number): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest(
      'POST',
      APP_CONFIG_VALUE.apiEndPoint + '/contadores',
      {contLista: lista, contPreferido: prefe},
      {headers: headers}
    );
    const response: any = await this.http.request(req).toPromise();
    console.log('Respuesta tras enviar contadores: ', response);
  }
}
// fin servicio que actualiza contadores

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 20
    }),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService,
    UsuarioLogueadoGuard,
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    AppLoadService,
    ContadoresService,
    {provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
    {provide: APP_INITIALIZER, useFactory: init_app2, deps: [ContadoresService], multi: true},
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
