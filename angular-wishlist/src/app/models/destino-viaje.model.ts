export class DestinoViaje{
    //private selected: boolean;
    public servicios: string[];

    constructor(
        public nombre: string,
        public imagenURL: string,
        public votes: number = 0,
        public selected: boolean = false
    ){
        this.servicios = ['wi-fi','desayuno'];
    }

    isSelected(): boolean{
        return this.selected;
    }

    setSelected(s: boolean) {
        this.selected = s;
    }

    public VoteUp() {
        console.log('Acabo de incrementar el votes a: ', this.votes);
        this.votes++;
    }

    VoteDown() {
        console.log('Acabo de decrementar el votes a: ', this.votes);
        this.votes--;
    }
}
