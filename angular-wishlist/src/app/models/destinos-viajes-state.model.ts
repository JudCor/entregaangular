import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { DestinoViaje } from './destino-viaje.model';

// ESTADO
export interface DestinosViajesState{
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
    //destino_vote_up: number;
    //destino_vote_down: number;
    lista_destinos_item: number;
    destino_elegir_preferido: number;
}

//esta función la puedo declarar de la siguiente manera comentada, y equivale a la manera descomentada
//export const initializeDestinosViajesState = function(){
export function initializeDestinosViajesState() {    
    //inicializo vacío el state porque esta función es sincrónica, se ejecuta nada más inicializarse
    //la app. Y luego cargo la info de manera asincrónica mediante el token reservado de Angular
    //APP_INITIALIZER, que llama a una función de inicialización que he creado, que devuelve un Promise
    return {
        items: [],
        loading: false,
        favorito: null,
        lista_destinos_item: 0,
        destino_elegir_preferido: 0
    };
};

//ACCIONES
export enum DestinosViajesActionTypes{
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote up',
    VOTE_DOWN = '[Destinos Viajes] Vote down',
    RESET_VOTES = '[Destinos Viajes] Reset votes',
    INIT_MY_DATA = '[Destinos Viajes] Init my data',
    INCREMENT_CLICKS = '[Destinos Viajes] Increment clicks'
}

export class NuevoDestinoAction implements Action{
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje){
        console.log('-Redux- Nuevo destino: ', destino.nombre);
    }
}

export class ElegidoFavoritoAction implements Action{
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje){
        console.log('-Redux- Elegido favorito: ', destino.nombre);
    }
}

export class VoteUpAction implements Action{
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje){
        console.log('-Redux- Vote up: ', destino.nombre);
    }
}

export class VoteDownAction implements Action{
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje){
        console.log('-Redux- Vote down: ', destino.nombre);
    }
}

export class ResetVotesAction implements Action{
    type = DestinosViajesActionTypes.RESET_VOTES;
    constructor(public destino: DestinoViaje){
        console.log('-Redux- Reset votes: ', destino.nombre);
    }
}

export class InitMyDataAction implements Action{
    type = DestinosViajesActionTypes.INIT_MY_DATA;
    //constructor(public destinos: string[]){}
    constructor(public destinos: DestinoViaje[]){}
}

export class IncrementClicksAction implements Action{
    type = DestinosViajesActionTypes.INCREMENT_CLICKS;
    constructor(public tag: string){}
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction 
    | VoteUpAction | VoteDownAction | ResetVotesAction | InitMyDataAction
    | IncrementClicksAction;

// REDUCERS
export function reducerDestinosViajes (
    state: DestinosViajesState,
    action: DestinosViajesActions
) : DestinosViajesState {
    console.log('Acción tipo -> ', action.type);
    switch (action.type) {
        case DestinosViajesActionTypes.INIT_MY_DATA: {
            return {
                ...state,
                //items: (action as InitMyDataAction).destinos.map(d => new DestinoViaje(d, ''))
                items: (action as InitMyDataAction).destinos.map(d => new DestinoViaje(d.nombre, d.imagenURL))
            };
        }
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
            //state.items.forEach(x => x.setSelected(false));
            const fav: DestinoViaje = Object.assign({}, (action as ElegidoFavoritoAction).destino);
            //fav.setSelected(true);
            fav.selected = true;

            return {
                ...state,
                favorito: fav,
                items: state.items.map(item => {
                    if (item === (action as ElegidoFavoritoAction).destino) {
                        item = fav;
                    } else {
                        const aux: DestinoViaje = Object.assign({}, item);
                        aux.selected = false;
                        item = aux;
                    }
                    return item;
                })
            }
        }
        case DestinosViajesActionTypes.VOTE_UP: {
            const d: DestinoViaje = Object.assign({}, (action as VoteUpAction).destino);
            console.log(`He dado like a ${d.nombre} y tengo: ${state.items}`);
            d.votes++;
            //d.VoteUp();
            return {
                ...state,
                items: [
                    ...state.items.slice(0, state.items.indexOf((action as VoteUpAction).destino)),
                    d,
                    ...state.items.slice(state.items.indexOf((action as VoteUpAction).destino) + 1)
                ]
            };
        }
        case DestinosViajesActionTypes.VOTE_DOWN: {
            const d: DestinoViaje = Object.assign({}, (action as VoteDownAction).destino);
            console.log('He dado dislike a ', d.nombre);
            d.votes--;
            //d.VoteDown();
            return {
                ...state,
                items: [
                    ...state.items.slice(0, state.items.indexOf((action as VoteUpAction).destino)),
                    d,
                    ...state.items.slice(state.items.indexOf((action as VoteUpAction).destino) + 1)
                ]
            };
        }
        case DestinosViajesActionTypes.RESET_VOTES: {
            return {
                ...state,
                items: state.items.map(item => {
                    if (item === (action as ResetVotesAction).destino) {
                        const aux: DestinoViaje = Object.assign({}, item);
                        aux.votes = 0;
                        item = aux;
                    }
                    return item;
                })
            };
        }
        case DestinosViajesActionTypes.INCREMENT_CLICKS: {
            switch ((action as IncrementClicksAction).tag) {
                case 'lista_destinos_item': {
                    console.log('///voy a incrementar lista///');
                    let cont_lista: number = state.lista_destinos_item; 
                    cont_lista++;
                    return {
                        ...state,
                        lista_destinos_item: cont_lista
                    };
                }
                case 'destino_elegir_preferido': {
                    console.log('///voy a incrementar prefe///');
                    let cont_prefe: number = state.destino_elegir_preferido; 
                    cont_prefe++;
                    return {
                        ...state,
                        destino_elegir_preferido: cont_prefe
                    };
                }
            }
            return state;
        }
    }
    return state;
}

// EFFECTS
@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions){}
}
