import { DestinosViajesState, initializeDestinosViajesState, InitMyDataAction, reducerDestinosViajes, NuevoDestinoAction, ElegidoFavoritoAction, VoteUpAction, VoteDownAction, ResetVotesAction, IncrementClicksAction } from "./destinos-viajes-state.model";
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
    // testeo el reducer tras la acción de inicialización
    it('should reduce init data', () => {
        // setup (declaro los objetos que voy a necesitar para testear)
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction([
            new DestinoViaje('destino 1', ''),
            new DestinoViaje('destino 2', '')
        ]);
        // action (accionar sobre el código productivo)
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        // assert o assertions (lo que esperamos como resultado del action)
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
        // tear down (volver atrás las acciones colaterales de ejecutar este test)
        // por ejemplo: borramos los datos que se han generado tras ejecutar el test
        // en este caso, como no persistimos en una BBDD, no es necesario borrar la basura generada
    });

    // testeo el reducer tras la acción de agregar
    it('should reduce new item added', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Ceuta', 'url'));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('Ceuta');
    });

    // testeo el reducer tras la acción de elegir favorito
    it('should reduce elegir favorito', () => {
        let prevState: DestinosViajesState = initializeDestinosViajesState();
        
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Ceuta', 'url'));
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        prevState = newState;
        
        const action2: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Melilla', 'url2'));
        newState = reducerDestinosViajes(prevState, action2);
        prevState = newState;

        const action3: ElegidoFavoritoAction = new ElegidoFavoritoAction(new DestinoViaje('Melilla', 'url'));
        newState = reducerDestinosViajes(prevState, action3);
        expect(newState.items.length).toEqual(2);
        expect(newState.favorito.nombre).toEqual('Melilla');
    });

    // testeo el reducer tras la acción de vote up
    it('should reduce vote up', () => {
        const d = new DestinoViaje('Melilla', 'url2');
        let prevState: DestinosViajesState = initializeDestinosViajesState();
        
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Ceuta', 'url'));
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        prevState = newState;
        
        const action2: NuevoDestinoAction = new NuevoDestinoAction(d);
        newState = reducerDestinosViajes(prevState, action2);
        prevState = newState;

        const action3: VoteUpAction = new VoteUpAction(d);
        newState = reducerDestinosViajes(prevState, action3);

        expect(newState.items.length).toEqual(2);
        expect(newState.items[1].votes).toEqual(1);
    });

    // testeo el reducer tras la acción de vote down
    it('should reduce vote down', () => {
        const d = new DestinoViaje('Melilla', 'url2');
        let prevState: DestinosViajesState = initializeDestinosViajesState();
        
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Ceuta', 'url'));
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        prevState = newState;
        
        const action2: NuevoDestinoAction = new NuevoDestinoAction(d);
        newState = reducerDestinosViajes(prevState, action2);
        prevState = newState;

        const action3: VoteDownAction = new VoteDownAction(d);
        newState = reducerDestinosViajes(prevState, action3);

        expect(newState.items.length).toEqual(2);
        expect(newState.items[1].votes).toEqual(-1);
    });

    // testeo el reducer tras la acción de reset votes
    it('should reduce reset votes', () => {
        const d = new DestinoViaje('Melilla', 'url2', 3);
        let prevState: DestinosViajesState = initializeDestinosViajesState();
        
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Ceuta', 'url', 5));
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        prevState = newState;
        
        const action2: NuevoDestinoAction = new NuevoDestinoAction(d);
        newState = reducerDestinosViajes(prevState, action2);
        prevState = newState;

        const action3: ResetVotesAction = new ResetVotesAction(d);
        newState = reducerDestinosViajes(prevState, action3);

        expect(newState.items.length).toEqual(2);
        expect(newState.items[1].votes).toEqual(0);
    });

    // testeo el reducer tras la acción de incrementar contadores
    it('should reduce incrementar contadores', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: IncrementClicksAction = new IncrementClicksAction('destino_elegir_preferido');
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        newState = reducerDestinosViajes(newState, action);
        expect(newState.items.length).toEqual(0);
        expect(newState.destino_elegir_preferido).toEqual(2);
    });
});
