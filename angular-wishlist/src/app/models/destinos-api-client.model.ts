import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

import { Store } from '@ngrx/store';

import { Subject, BehaviorSubject } from 'rxjs';

import { DestinoViaje } from './destino-viaje.model';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';

@Injectable()
export class DestinosApiClient{
    //private destinos: DestinoViaje[];
    current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
    all: DestinoViaje[];

    constructor(
        private store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http: HttpClient
    ) {
        /*this.destinos = [];
        this.store
            .select(state => state.destinos)
            .subscribe((data) => {
                console.log('destinos sub store');
                console.log(data);
                this.destinos = data.items;
            });
        this.store
            .subscribe((data) => {
                console.log('all store');
                console.log(data);
            });*/
    }

    /*add(destino: DestinoViaje) {
        console.log(`Nombre: ${destino.nombre}, URL: ${destino.imagenURL}`);
        //this.destinos.push(destino);
        this.store.dispatch(new NuevoDestinoAction(destino));
    }*/
    add(destino: DestinoViaje) {
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'}); //envío unas credenciales ficticias porque en la api no estoy validando login y siempre logeo
        const req = new HttpRequest(
            'POST',
            this.config.apiEndPoint + '/my',
            {nuevo: destino.nombre},
            {headers: headers}
        ); //envío request
            //tipo POST, 
            //a mi apiEndPoint,
            //con un body json {nuevo: nombre} que envía una variable "nuevo" que luego usará la api para recoger su valor,
            //y el header
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                this.store.dispatch(new NuevoDestinoAction(destino));
                const myDb = db;
                myDb.destinos.add(destino);
                console.log('Todos los destinos de la db!');
                myDb.destinos.toArray().then(destinos => console.log(destinos)); //toArray es como hacer una consulta a la IndexedDB y devuelve una promesa, y then es cuando nos llega la respuesta
            }
        });
    }

    getAll(): DestinoViaje[] {
        //return this.destinos;
        this.store.select(state => state.destinos).subscribe(data => this.all = data.items);
        return this.all;
    }

    getById(id: number): DestinoViaje {
        let d: DestinoViaje;
        this.store.select(state => state.destinos).subscribe(data => d = data.items[id]);
        return d;
    }

    getId(d: DestinoViaje): number {
        let id: number;
        this.store.select(state => state.destinos).subscribe(data => id = data.items.indexOf(d));
        return id;
    }

    /*getById(id: string): DestinoViaje {
        return this.destinos.filter(function(d) {return d.id.toString() === id; })[0];
    }*/

    elegir(d: DestinoViaje) {
        // this.destinos.forEach(x => x.setSelected(false));
        // d.setSelected(true);
        console.log('-Api Client- He elegido a: ', d.nombre);
        this.store.dispatch(new ElegidoFavoritoAction(d));
        // this.current.next(d);
    }

    subscribeOnChange(fn) {
        this.current.subscribe(fn);
    }
}
