import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';

import { Store } from '@ngrx/store';

import { DestinoViaje } from '../../models/destino-viaje.model';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction, ResetVotesAction } from '../../models/destinos-viajes-state.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('* => estadoFavorito', [
        animate('3s')
      ]),
      transition('* => estadoNoFavorito', [
        animate('4s')
      ])
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input() position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';  //esto es para que, al generarse el componente ficticio en el html en ejecución, coja la clase col-md-4 y así salgan las fichas una al lado de la otra
  @Output() clicked: EventEmitter<DestinoViaje>;
  myID: number;

  constructor(private store: Store<AppState>, private apiCli: DestinosApiClient) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
    this.myID = this.apiCli.getId(this.destino);
  }

  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  VoteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  VoteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

  Reset(){
    this.store.dispatch(new ResetVotesAction(this.destino));
    return false;
  }

}
