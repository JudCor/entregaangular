import { Component, OnInit, InjectionToken, Inject, Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { Store } from '@ngrx/store';

import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { AppState } from '../../app.module';

export class DestinosApiClientViejo {
  getById(id:number): DestinoViaje {
    console.log('Llamando por la clase vieja!');
    return null;
  }
}

interface AppConfig {
  apiEndpoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

/*@Injectable()
export class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(
    store: Store<AppState>,
    @Inject(APP_CONFIG) private config: AppConfig,
    http: HttpClient
  ) {
    super(store, config, http);
  }
  getById(id: number): DestinoViaje {
    console.log('Llamando por la clase decorada!');
    console.log('Config: ' + this.config.apiEndpoint);
    return super.getById(id);
  }
}*/

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    /*{provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    {provide: DestinosApiClient, useClass: DestinosApiClientDecorated},
    {provide: DestinosApiClientViejo, useExisting: DestinosApiClient}*/
    DestinosApiClient
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  estiloMapa = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    center: [0, 0],
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  //constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClientViejo) { }
  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) { }

  ngOnInit(): void {
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.destino = this.destinosApiClient.getById(id);
  }

}
