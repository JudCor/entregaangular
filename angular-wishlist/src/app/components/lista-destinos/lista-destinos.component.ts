import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Store } from '@ngrx/store';

import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})

export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];

  /*constructor(public destinosApiClient: DestinosApiClient) {
    //this.onItemAdded = new EventEmitter();
    this.updates = [];
    destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
      if (d != null) {
        this.updates.push('Se ha elegido a ' + d.nombre);
      }
    });
  }*/

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    //this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        if (data != null) {
          this.updates.push('Se ha elegido a ' + data.nombre);
        }
      });
  }

  ngOnInit(): void {
  }

/*  guardar(n: string, u: string): boolean {
    console.log(`Nombre ${n}, URL ${u}`);
    this.destinos.push(new DestinoViaje(n,	u));
    console.log(this.destinos);
    return false; //devolviendo false, consigo que no se recargue la página por el clic del botón (el clic de un botón recarga la página a no ser que reciba un false)
  }*/

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    //this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(d: DestinoViaje) {
    //this.destinos.forEach(function (x) { x.setSelected(false); });
    //d.setSelected(true);
    
    //this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //d.setSelected(true);
    this.destinosApiClient.elegir(d);
    //this.store.dispatch(new ElegidoFavoritoAction(d));
  }

}
