import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';

import { TranslateService, TranslateModule } from '@ngx-translate/core';

import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';

import { DestinoViaje } from '../../models/destino-viaje.model';
import { APP_CONFIG, AppConfig } from '../../app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(
    fb: FormBuilder,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig
  ) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['', Validators.required]
    });
    
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Cambio el formulario: ', form);
    });
  }

  /*ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input') //fromEvent crea un observable que analiza el evento input sobre el elemento elemNombre
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value), //el stream analiza todo el texto cada vez que se pulsa una tecla
        filter(text => text.length > 2), //el stream sólo analiza el texto si es mayor a 3 caracteres
        debounceTime(200), //dejamos 2 décimas de segundo porque el tecleo puede ser muy rápido y puede ralentizarse. Es decir, no analizamos cada tecleo al momento, sino que en 2 décimas de segundo puede que se haya tecleado 3 o 4 teclas por ejemplo
        distinctUntilChanged(), //sólo emite si el valor es distinto del valor que se analizó hace 2 décimas de segundo. Por ejemplo, desde la última comprobación, pulso una tecla y la borro inmediatamente, entonces no se emite cambio porque este operador lo descarta al ser el texto el mismo que en la última comprobación. Esto es muy útil si este stream por ejemplo emitiera una request HTTP, entre el debounceTime y el distinctUntilChange puedo evitar enviar solicitudes constantemente cada vez que pulsan una tecla
        switchMap(() => ajax('/assets/datos.json')) //el texto se lo pasamos a una api web service para que lo busque, aunque esto es una simulación (el datos.json ése es un fichero que yo he creado de prueba para devolver siempre un array de strings como si fuera el ws el que me lo responde)
      ).subscribe((ajaxResponse: AjaxResponse) => { //nos subscribimos a la respuesta que nos mande la api
        this.searchResults = ajaxResponse.response;
      })
  }*/

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input') //fromEvent crea un observable que analiza el evento input sobre el elemento elemNombre
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value), //el stream analiza todo el texto cada vez que se pulsa una tecla
        filter(text => text.length > 2), //el stream sólo analiza el texto si es mayor a 3 caracteres
        debounceTime(200), //dejamos 2 décimas de segundo porque el tecleo puede ser muy rápido y puede ralentizarse. Es decir, no analizamos cada tecleo al momento, sino que en 2 décimas de segundo puede que se haya tecleado 3 o 4 teclas por ejemplo
        distinctUntilChanged(), //sólo emite si el valor es distinto del valor que se analizó hace 2 décimas de segundo. Por ejemplo, desde la última comprobación, pulso una tecla y la borro inmediatamente, entonces no se emite cambio porque este operador lo descarta al ser el texto el mismo que en la última comprobación. Esto es muy útil si este stream por ejemplo emitiera una request HTTP, entre el debounceTime y el distinctUntilChange puedo evitar enviar solicitudes constantemente cada vez que pulsan una tecla
        switchMap((text: string) => ajax(this.config.apiEndPoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse => {
        this.searchResults = ajaxResponse.response;
        console.log('resultado consulta: ', this.searchResults);
      })
  }

  guardar(nombre: string, url: string): boolean {
    console.log('Guardando...');
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;

    if(l > 0 && l < 5) {
      return { invalidNombre: true };
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if(l > 0 && l < minLong) {
        return { minLongNombre: true };
      }
      return null;
    }
  }

}
