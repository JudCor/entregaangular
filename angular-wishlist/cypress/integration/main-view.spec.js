import { createYield } from "typescript";

describe('Ventana principal', () => {
    it('Tiene encabezado correcto y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular-wishlist');  //contains busca en todo el texto pintado en la página
        //cy.get('h1 b').should('contain', 'Nombre');  //get busca en un tag b, dentro de un tag h1
        cy.get('h1').should('contain', 'Nombre');
    });

    it('Puedo cambiar idioma inglés y cambia texto', () => {
        cy.visit('http://localhost:4200');
        cy.get('select').should('have.value', 'es');
        cy.get('select').select('English');
        cy.contains('Image URL');
        cy.get('h1').should('contain', 'Name');
    });

    it('Voy a la sección de reservas', () => {
        cy.visit('http://localhost:4200');
        /*cy.get('a').each(data => {
            if (data.index===1) {
                data.click();
            }
        });*/
        cy.get('a').contains('Reservas').click();
        cy.get('a').contains('login').click();
        cy.contains('Entrar!');
    });
});
