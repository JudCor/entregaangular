var express = require("express"), cors = require("cors");
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var ciudades = ["Valencia","Alicante","Castellón","Mallorca","Sevilla","Palencia"];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter(c => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    //misDestinos = req.body;
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});

var misCanciones = [];
app.get("/songs", (req, res, next) => res.json(misCanciones));
app.post("/songs", (req, res, next) => {
    console.log(req.body);
    misCanciones.push({titulo: req.body.titulo, artista: req.body.artista, genero: req.body.genero});
    res.json(misCanciones);
});

var traducs = [];
app.get("/api/translation", (req, res, next) => {
    switch (req.query.lang) {
        case 'es':
            traducs = [
                {lang: 'es', key: 'ruteo', value: 'Ruteo simple'},
                {lang: 'es', key: 'url', value: 'URL de la imagen'},
                {lang: 'es', key: 'nombre', value: 'Nombre'}
            ];
            break;
        case 'en':
            traducs = [
                {lang: 'en', key: 'ruteo', value: 'Simple routing'},
                {lang: 'en', key: 'url', value: 'Image URL'},
                {lang: 'en', key: 'nombre', value: 'Name'}
            ];
            break;
        case 'fr':
            traducs = [
                {lang: 'fr', key: 'ruteo', value: 'Le simpleé routeé'},
                {lang: 'fr', key: 'url', value: 'La URL de la imageée'},
                {lang: 'fr', key: 'nombre', value: 'Le nomée'}
            ];
            break;
    }
    /*res.json([
        {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
    ])*/
    res.json(traducs);
});
